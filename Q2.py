from os.path import join
from os import getcwd

from scipy.ndimage import imread

from matplotlib import pyplot as plt, colors
from mpl_toolkits.axes_grid1 import make_axes_locatable

import recalage.metrics as mtrx
from recalage.utils import rgb_to_grayscale

project_path = getcwd()
image_path = "Data"

images_prefixes = ["I", "J"]
images_numbers = [i for i in range(1, 7)]
n_images = len(images_numbers)
images_extensions = [".png"] + [".jpg" for i in range(n_images - 1)]

histogram = None
bins = 150


def im_wrapper(image1, image2):
    return mtrx.im(histogram)


def ssd_wrapper(image1, image2):
    return mtrx.ssd(image1, image2, 2)


metrics = {
    "ssd": ssd_wrapper,
    "cr": mtrx.cr,
    "im": im_wrapper
}

for i_nb, ext in zip(images_numbers, images_extensions):

    plot_string = ""

    image1, image2 = [imread(join(project_path, image_path, "".join([prf, str(i_nb), ext]))) for prf in images_prefixes]

    if len(image1.shape) > 2:
        image1 = rgb_to_grayscale(image1)
    if len(image2.shape) > 2:
        image2 = rgb_to_grayscale(image2)

    histogram, i1_max, i2_max = mtrx.jointHist(image1, image2, 150)

    for id, metric in metrics.items():
        plot_string += id + " : {:.5f}    ".format(metric(image1, image2))

    fig, axe = plt.subplots()

    divider = make_axes_locatable(axe)
    caxe = divider.append_axes("right", size="5%", pad=0.05)

    axe.set_title("Histogramme conjoint")
    pcm = axe.pcolormesh(
        histogram,
        cmap=plt.cm.jet,
        norm=colors.SymLogNorm(vmin=0, vmax=histogram.max(), linthresh=0.03, linscale=0.3),
    )

    axe.set_xlabel("Image 2\n" + plot_string)
    axe.set_ylabel("Image 1")

    axe.xaxis.set_major_formatter(plt.FuncFormatter(lambda v, t: int(v * i1_max / bins) if t else 0))
    axe.yaxis.set_major_formatter(plt.FuncFormatter(lambda v, t: int(v * i2_max / bins) if t else 0))
    axe.set_aspect('equal', 'box')
    axe.set_xbound(0, bins)
    axe.set_ybound(0, bins)

    ax2 = divider.new_vertical(size="100%", pad=0.5)
    ax3 = divider.new_vertical(size="100%", pad=0.5)
    fig.add_axes(ax2)
    fig.add_axes(ax3)

    ax2.set_title("Image 2")
    ax3.set_title("Image 1")
    ax2.imshow(image2, cmap="gray", origin="upper")
    ax3.imshow(image1, cmap="gray", origin="upper")
    ax2.set_aspect('equal', 'box')
    ax3.set_aspect('equal', 'box')

    fig.colorbar(pcm, cax=caxe)

    fig.tight_layout()

    plt.axis("equal")

    plt.show()
