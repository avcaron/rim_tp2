from mpl_toolkits.mplot3d import Axes3D

import matplotlib.pyplot as plt
from math import pi
from numpy import array

from recalage.utils import linspace_nd, to_hom_coords, from_hom_coords
from recalage.transformation import trans_rigide, similitude

# Numero 3 a) a c)
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1, projection='3d')

intervals = (20, 20, 5)
steps = (1, 1, 1)

points = linspace_nd(intervals, steps=steps)

trans = similitude(1, pi / 4., pi / 4., pi / 4., 0, 0, 0)

pointsTransformed = from_hom_coords(trans @ to_hom_coords(points))

ax.scatter(*points, color=(0, 0, 0), marker='o')
ax.scatter(*pointsTransformed, color=(0, 1, 0), marker='o')
plt.show()

# Numero 3 d)
fig = plt.figure()
axes = [fig.add_subplot(1, 3, i + 1, projection='3d') for i in range(3)]

M1 = array([
    [0.9045, -0.3847, -0.1840, 10],
    [0.2939, 0.8750, -0.3847, 10],
    [0.3090, 0.02939, 0.9045, 10],
    [0, 0, 0, 1]
])
M2 = array([
    [0, -0.2598, 0.15, -3],
    [0, -0.15, -0.2598, 1.5],
    [0.3, 0, 0, 0],
    [0, 0, 0, 1]
])
M3 = array([
    [0.7182, -1.3727, -0.5660, 1.8115],
    [-1.9236, -4.6556, -2.5512, 0.2873],
    [-0.6426, -1.7985, -1.6285, 0.7404],
    [0, 0, 0, 1]
])

points = linspace_nd([20, 20, 5], steps=(1, 1, 1))

for axe, transformation in zip(axes, [M1, M2, M3]):

    pointsTransformed = from_hom_coords(transformation @ to_hom_coords(points))

    axe.scatter(*points, color=(0, 0, 0), marker='o')
    axe.scatter(*pointsTransformed, color=(0, 1, 0), marker='o')

plt.show()
