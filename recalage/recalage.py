from numpy import gradient, sum, array
from matplotlib import pyplot as plt
from math import fabs, pi, sin, cos

from .metrics import ssd
from .transformation import rotation_image, translation_image, translation_cm
from .utils import linspace_nd


def recalage_2d_rotation(fixed, moving):
    eps = 5E-12
    image_temp = moving
    theta = 0
    # Initialisation des SSD
    ssd_curr = 0
    ssd_prec = 100000
    liste_ssd = []
    # Minimiser SSD jusqu'a diff entre les SDDs minimale
    while fabs(ssd_curr - ssd_prec) > 1:
        deriv = gradient(image_temp)

        a = array([
            [
                [-x * sin(theta) - y * cos(theta) for y in range(array(deriv).shape[2])],
                [x * cos(theta) - y * sin(theta) for y in range(array(deriv).shape[2])]
            ]
            for x in range(array(deriv).shape[1])
        ]).swapaxes(0, 1)

        deriv = sum(deriv * a, axis=0)
        # Theta(i+1) = Theta(i) - eps*(d(SSD)/d(theto))
        theta += 2. * eps * ssd(fixed * deriv, image_temp * deriv)
        # Appliquer la nouvelle rotation
        image_temp = rotation_image(moving, theta)
        # Calculer nouvel SSD
        ssd_prec = ssd_curr
        liste_ssd.append(ssd_curr)
        ssd_curr = ssd(image_temp, fixed, 2)
        # Sauver l'energie qui correspond a ch etat
        print(fabs(ssd_curr - ssd_prec))
        print('theta = ' + str(theta))


    # Visualiser le recalage final, la courbe d'energie et utiliser un mesure pour quantifier la qualite
    fig, ax = plt.subplots(2, 2)
    ax[0, 0].set_title('Image Fixed')
    ax[0, 1].set_title('Image Moving')
    ax[1, 0].set_title('Image Resultante')
    ax[1, 1].set_title('Difference')
    fig.text(0.5, 0.05, 'Theta final = ' + str(180. * theta / pi), ha='center')
    ax[0, 0].imshow(fixed, cmap='gray')
    ax[0, 1].imshow(moving, cmap='gray')
    ax[1, 0].imshow(image_temp, cmap='gray')
    ax[1, 1].imshow(image_temp - fixed, cmap='gray')

    plt.show()

    fig, ax = plt.subplots()
    ax.plot(linspace_nd([len(liste_ssd)], steps=[1])[0], liste_ssd)
    ax.set_title('Descente du ssd')

    plt.show()


def recalage_2d_translation(fixed, moving, eps):
    image_temp, translation = translation_cm(fixed, moving)

    p, q = translation[0], translation[1]
    # Initialisation des SSD
    ssd_curr = 0
    ssd_prec = 100000
    liste_ssd = []
    # Minimiser SSD jusqu'a diff entre les SDDs minimale
    while fabs(ssd_curr - ssd_prec) > 1:
        deriv = gradient(image_temp)
        # p(i+1) = pi - eps*(d(SSD)/d(p))

        p -= 2. * eps * ssd(fixed * deriv[0], image_temp * deriv[0])
        q -= 2. * eps * ssd(fixed * deriv[1], image_temp * deriv[1])
        # Appliquer la nouvelle translation
        image_temp = translation_image(moving, p, q)
        # Calculer nouvel SSD
        ssd_prec = ssd_curr
        liste_ssd.append(ssd_curr)
        ssd_curr = ssd(image_temp, fixed, 2)
        print(fabs(ssd_curr - ssd_prec))
        print("translation {0} | {1}".format(p, q))

    # Visualiser le recalage final, la courbe d'energie et utiliser un mesure pour quantifier la qualite
    fig, ax = plt.subplots(2, 2)
    ax[0, 0].set_title('Image Fixed')
    ax[0, 1].set_title('Image Moving')
    ax[1, 0].set_title('Image Resultante')
    ax[1, 1].set_title('Difference')
    fig.text(0.5, 0.05, 'p = ' + str(p) + '  q = ' + str(q), ha='center')

    ax[0, 0].imshow(fixed, cmap='gray')
    ax[0, 1].imshow(moving, cmap='gray')
    ax[1, 0].imshow(image_temp, cmap='gray')
    ax[1, 1].imshow(image_temp - fixed, cmap='gray')

    plt.show()

    fig, ax = plt.subplots()
    ax.plot(linspace_nd([len(liste_ssd)], steps=[1])[0], liste_ssd)
    ax.set_title('Descente du ssd')

    plt.show()
