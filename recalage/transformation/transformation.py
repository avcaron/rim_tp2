from numpy import zeros
from scipy.interpolate import RectBivariateSpline
from math import cos, sin

from .translation import get_translation
from .rotation import get_rotation, Axes
from recalage.utils import linspace_nd


def trans_rigide(theta, omega, phi, p, q, r):
    return get_translation(p, q, r) @ get_rotation((Axes.X, Axes.Y, Axes.Z), (theta, omega, phi))


def similitude(s, theta, omega, phi, p,q, r):

    # scaling
    scale = zeros((4, 4))
    scale[0, 0] = scale[1, 1] = scale[2, 2] = s
    scale[3, 3] = 1

    return scale @ trans_rigide(theta, omega, phi, p, q, r)


def rigide_image(image, p, q, theta):
    # rotation theta au centre de l'image
    rows, cols = image.shape[0], image.shape[1]
    center = [rows / 2., cols / 2.]

    interpolator = RectBivariateSpline(range(rows), range(cols), image)
    grid = linspace_nd([rows, cols], steps=[1, 1])

    return interpolator.ev(
        (grid[0] - center[0]) * cos(theta) - (grid[1] - center[1]) * sin(theta) + center[0] + p,
        (grid[1] - center[1]) * cos(theta) + (grid[0] - center[0]) * sin(theta) + center[1] + q
    ).reshape(rows, cols)
