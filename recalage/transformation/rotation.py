from math import sin, cos
from numpy import array, apply_along_axis
from scipy.interpolate import RectBivariateSpline
from functools import reduce
from enum import Enum

from recalage.utils import linspace_nd

ROTATION_X = lambda theta: [
        [1, 0, 0, 0],
        [0, cos(theta), -sin(theta), 0],
        [0, sin(theta), cos(theta), 0],
        [0, 0, 0, 1]
    ]

ROTATION_Y = lambda omega: [
        [cos(omega), 0, -sin(omega), 0],
        [0, 1, 0, 0],
        [sin(omega), 0, cos(omega), 0],
        [0, 0, 0, 1]
    ]

ROTATION_Z = lambda phi: [
        [cos(phi), -sin(phi), 0, 0],
        [sin(phi), cos(phi), 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]
    ]


class Axes(Enum):
    X = ROTATION_X
    Y = ROTATION_Y
    Z = ROTATION_Z


def get_rotation(axes, angles):
    assert len(axes) == len(angles)
    return reduce(lambda a, b: array(a) @ array(b), [r[0](r[1]) for r in zip(axes[::-1], angles[::-1])])


def rotation_image(image, theta):
    # rotation theta au centre de l'image
    rows, cols = image.shape[0], image.shape[1]
    center = [rows / 2., cols / 2.]

    interpolator = RectBivariateSpline(range(rows), range(cols), image)
    grid = linspace_nd([rows, cols], steps=[1, 1])

    return interpolator.ev(
        (grid[0] - center[0]) * cos(theta) + (grid[1] - center[1]) * sin(theta) + center[0],
        (grid[1] - center[1]) * cos(theta) - (grid[0] - center[0]) * sin(theta) + center[1]
    ).reshape(rows, cols)
