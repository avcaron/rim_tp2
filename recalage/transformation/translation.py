from numpy import array
from scipy.interpolate import RectBivariateSpline
from scipy.ndimage.measurements import center_of_mass

from recalage.utils import linspace_nd


TRANSLATION = lambda x: [
        [1, 0, 0, x[0]],
        [0, 1, 0, x[1]],
        [0, 0, 1, x[2]],
        [0, 0, 0, 1]
    ]


def get_translation(x, y, z):
    return TRANSLATION([x, y, z])


def translation_cm(fixed, moving):
    translation = array(center_of_mass(moving)) - array(center_of_mass(fixed))
    return translation_image(moving, *translation), translation


def translation_image(image, p, q):
    # translate image by (p,q)
    rows, cols = image.shape[0], image.shape[1]

    interpolator = RectBivariateSpline(range(rows), range(cols), image)
    grid = linspace_nd([rows, cols], steps=[1, 1])

    return interpolator.ev(grid[0] + p, grid[1] + q).reshape(rows, cols)
