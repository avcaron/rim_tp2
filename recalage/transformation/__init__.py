from .transformation import trans_rigide, similitude, rigide_image
from .rotation import rotation_image
from .translation import translation_image, translation_cm
