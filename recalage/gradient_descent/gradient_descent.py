from numpy import gradient, sum, array, mean, var
from matplotlib import pyplot as plt
from math import fabs, pi, sin, cos, sqrt

from recalage.metrics import ssd
from recalage.transformation import rigide_image, translation_cm
from recalage.utils import linspace_nd


def descente_gradient(fixed, moving, fenetre_ssd=20, eps_ssd=1000):
    eps_trans = 5E-8
    eps_rot = 5E-12
    # Initialiser la transformation
    image_temp, translation = translation_cm(fixed, moving)
    p, q = translation[0], translation[1]
    theta = 0

    fig, ax = plt.subplots(1, 3)
    ax[0].set_title('Image Origine')
    ax[1].set_title('Image Target')
    ax[2].set_title('Image Resultante')
    fig.suptitle('p = ' + str(p) + ' q = ' + str(q) + ' Theta final = ' + str(180. * theta / pi))
    ax[0].imshow(fixed, cmap='gray')
    ax[1].imshow(moving, cmap='gray')
    ax[2].imshow(image_temp, cmap='gray')
    plt.show()

    # initialisation des SSD
    ssd_curr = 0
    ssd_prec = 100000
    liste_ssd = [ssd(fixed, moving, 2)]

    oscillating = False
    # minimiser SSD jusqu'a diff entre les SDDs minimale
    while fabs(ssd_curr - ssd_prec) > 1 and not oscillating:

        deriv = gradient(image_temp)

        a = array([
                [
                    [-x * sin(theta) - y * cos(theta) for y in range(array(deriv).shape[2])],
                    [x * cos(theta) - y * sin(theta) for y in range(array(deriv).shape[2])]
                ]
                for x in range(array(deriv).shape[1])
         ]).swapaxes(0, 1)

        s_deriv = sum(deriv * a, axis=0)

        theta -= 2. * eps_rot * ssd(fixed * s_deriv, image_temp * s_deriv)
        image_temp = rigide_image(moving, p, q, theta)
        # p(i+1) = pi - eps*(d(SSD)/d(p))
        deriv = gradient(image_temp)
        p -= 2. * eps_trans * ssd(fixed * deriv[0], image_temp * deriv[0])
        q -= 2. * eps_trans * ssd(fixed * deriv[1], image_temp * deriv[1])
        # Theta(i+1) = Theta(i) - eps*(d(SSD)/d(theto))

        # Appliquer la nouvelle transformation
        image_temp = rigide_image(moving, p, q, theta)
        # sauver l'energie qui correspond a ch etat
        liste_ssd.append(ssd_curr)
        ssd_prec = ssd_curr
        # Calculer nouvel SSD
        ssd_curr = ssd(image_temp, fixed, 2)
        print(ssd_curr)
        print("translation {0} | {1}".format(p, q))
        print("rotation {0}".format(180. * theta / pi))

        if len(liste_ssd) >= fenetre_ssd and ssd_curr < ssd_prec:
            oscillating = abs(abs((ssd_prec + ssd_curr) / 2.) - abs(mean(array(liste_ssd[-fenetre_ssd::])))) <= eps_ssd

    # Visualiser le recalage final, la courbe d'energie et utiliser un mesure pour quantifier la qualite
    fig, ax = plt.subplots(2, 2)
    ax[0, 0].set_title('Image Origine')
    ax[0, 1].set_title('Image Target')
    ax[1, 0].set_title('Image Resultante')
    ax[1, 1].set_title('Difference')
    fig.suptitle('p = ' + str(p) + ' q = ' + str(q) + ' Theta final = ' + str(180. * theta / pi))
    ax[0, 0].imshow(fixed, cmap='gray')
    ax[0, 1].imshow(moving, cmap='gray')
    ax[1, 0].imshow(image_temp, cmap='gray')
    ax[1, 1].imshow(image_temp - fixed, cmap='gray')

    fig, ax = plt.subplots()
    ax.plot(linspace_nd([len(liste_ssd)], steps=[1])[0], liste_ssd)
    ax.set_title('Descente du ssd')

    plt.show()

# Parametres :
#   - fenetre_ssd : nombre d'echantillon de la ssd observes pour la determination d'oscillation
#   - eps_ssd : distance maximale entre le centre d'oscillation courant et la moyenne sur la fenetre_ssd
#   - gamma : terme de regularisation du pas variable
def descente_gradient_optim(fixed, moving, eps_rot=0.0005, eps_trans=0.05, fenetre_ssd=20, eps_ssd=1000, gamma=0.9):
    small_number = 2E-15
    # Initialiser la transformation
    image_temp, translation = translation_cm(fixed, moving)
    p, q = translation[0], translation[1]
    theta = 0
    # initialisation des SSD
    ssd_curr = 0
    ssd_prec = 100000
    liste_ssd = [ssd(fixed, moving, 2)]
    # RMSProp
    m1 = m2 = m3 = 0
    oscillating = False
    # minimiser SSD jusqu'a diff entre les SDDs minimale
    while fabs(ssd_curr - ssd_prec) > 1 and not oscillating:

        deriv = gradient(image_temp)
        a = array([
                [
                    [-x * sin(theta) - y * cos(theta) for y in range(array(deriv).shape[2])],
                    [x * cos(theta) - y * sin(theta) for y in range(array(deriv).shape[2])]
                ]
                for x in range(array(deriv).shape[1])
         ]).swapaxes(0, 1)
        # Theta(i+1) = Theta(i) - eps*(d(SSD)/d(theta))
        s_deriv = sum(deriv * a, axis=0)
        rotssd = ssd(fixed * s_deriv, image_temp * s_deriv)
        m1 = (gamma * m1) + (1 - gamma)*fabs(rotssd)
        theta -= 2.*(eps_rot / (m1 + small_number)) * rotssd
        image_temp = rigide_image(moving, p, q, theta)

        # p(i+1) = pi - eps*(d(SSD)/d(p))
        deriv = gradient(image_temp)
        ssdp = ssd(fixed * deriv[0], image_temp * deriv[0])
        ssdq = ssd(fixed * deriv[1], image_temp * deriv[1])
        m2 = (gamma * m2) + (1 - gamma) * fabs(ssdp)
        m3 = (gamma * m3) + (1 - gamma) * fabs(ssdq)
        p -= 2. * (eps_trans / (m2 + small_number)) * ssdp
        q -= 2. * (eps_trans / (m3 + small_number)) * ssdq

        # Appliquer la nouvelle transformation
        image_temp = rigide_image(moving, p, q, theta)
        # sauver l'energie qui correspond a ch etat
        ssd_prec = ssd_curr
        # Calculer nouvel SSD
        ssd_curr = ssd(image_temp, fixed, 2)
        liste_ssd.append(ssd_curr)
        print(fabs(ssd_curr - ssd_prec))
        print("translation {0} | {1}".format(p, q))
        print("rotation {0}".format(theta))

        if len(liste_ssd) >= fenetre_ssd and ssd_curr < ssd_prec:
            oscillating = abs(abs((ssd_prec + ssd_curr) / 2.) - abs(mean(array(liste_ssd[-fenetre_ssd::])))) <= eps_ssd

    # Visualiser le recalage final, la courbe d'energie et utiliser un mesure pour quantifier la qualite
    fig, ax = plt.subplots(2, 2)
    ax[0, 0].set_title('Image Fixed')
    ax[0, 1].set_title('Image Moving')
    ax[1, 0].set_title('Image Resultante')
    ax[1, 1].set_title('Difference')
    fig.text(0.5, 0.005, 'p = ' + str(p) + ' q = ' + str(q) + ' Theta final = ' + str(180. * theta / pi), ha='center')
    ax[0, 0].imshow(fixed, cmap='gray')
    ax[0, 1].imshow(moving, cmap='gray')
    ax[1, 0].imshow(image_temp, cmap='gray')
    ax[1, 1].imshow(image_temp - fixed, cmap='gray')

    fig, ax = plt.subplots()
    ax.plot(linspace_nd([len(liste_ssd)], steps=[1])[0], liste_ssd)
    ax.set_title('Descente du ssd')

    plt.show()