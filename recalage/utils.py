from numpy import mgrid, s_, prod as pd, multiply as mlt, reciprocal as inv, append, ones


def rgb_to_grayscale(image):
    return image.mean(axis=2)


def linspace_nd(maximum, minimum=None, steps=None):
    minimum = [0 for m in maximum] if minimum is None else minimum
    return mgrid[[s_[p[0]:p[1]:p[2]] for p in zip(minimum, maximum, steps)]
                 ].reshape(len(maximum), int(pd(mlt(inv(steps), maximum))))


def to_hom_coords(points):
    return append(points, [ones((points.shape[1]))], axis=0)


def from_hom_coords(points):
    return points[:3, :] / points[[-1], :]