from numpy import sum, log10, apply_along_axis as aaa, ones_like as ol, multiply as mlt, copy


def im(hist_normal):
    h = copy(hist_normal)
    h /= h.max()
    p = mlt(
        aaa(lambda x: ol(x) if sum(x) == 0 else (ol(x) / sum(x)), 1, copy(h)),
        aaa(lambda x: ol(x) if sum(x) == 0 else (x / sum(x)), 0, copy(h))
    )
    return abs(sum(mlt(h, log10(p, where=p > 0))))
