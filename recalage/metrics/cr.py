from numpy import sum, power

from math import sqrt


def cr(image1, image2):
    i_barre, j_barre = image1.mean(), image2.mean()

    return sum((image1 - i_barre) * (image2 - j_barre)) / \
           sqrt(sum(power(image1 - i_barre, 2)) * sum(power(image2 - j_barre, 2)))