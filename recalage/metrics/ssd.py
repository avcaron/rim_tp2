import numpy as np


def ssd(image1, image2, power=None):
    return np.sum(np.power(image2 - image1, power)) if power else np.sum(image2 - image1)
