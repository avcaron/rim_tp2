import numpy as np


def jointHist(image1, image2, bins):
    img1_max, img2_max = image1.max(), image2.max()

    hist = np.zeros([bins, bins])
    np.add.at(hist, [
        (image1.ravel() / img1_max * (bins - 1.)).astype(int),
        (image2.ravel() / img2_max * (bins - 1.)).astype(int)
    ], 1)

    return hist, img1_max, img2_max
