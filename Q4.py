import matplotlib.pyplot as plt
import os

from recalage import recalage_2d_rotation, recalage_2d_translation
from recalage.gradient_descent import descente_gradient, descente_gradient_optim
from recalage.transformation import rotation_image, translation_image
from recalage.utils import rgb_to_grayscale


def test_rotation():
    # load image
    image_filename = os.getcwd() + '/Data/BrainMRI_1.jpg'
    image = plt.imread(image_filename)
    if len(image.shape) > 2:
        image = rgb_to_grayscale(image)
    # Tester 3 rotations diff
    print("Rotation 1")
    recalage_2d_rotation(image, rotation_image(image, 0.2))
    print("Rotation 2")
    recalage_2d_rotation(image, rotation_image(image, -0.25))
    print("Rotation 3")
    recalage_2d_rotation(image, rotation_image(image, 0.1))


def test_translation():
    # load image
    image_filename = os.getcwd() + '/Data/BrainMRI_1.jpg'
    image = plt.imread(image_filename)
    if len(image.shape) > 2:
        image = rgb_to_grayscale(image)
    print("Translation 1")
    recalage_2d_translation(image, translation_image(image, -40, -40), 5E-8)
    print("Translation 2")
    recalage_2d_translation(image, translation_image(image, 1, 40), 5E-8)
    print("Translation 3")
    recalage_2d_translation(image, translation_image(image, 20, -20), 5E-8)


def test_recalage():
    image_filename1 = os.getcwd() + '/Data/BrainMRI_1.jpg'
    image_filename2 = os.getcwd() + '/Data/BrainMRI_2.jpg'
    image_filename3 = os.getcwd() + '/Data/BrainMRI_3.jpg'
    image_filename4 = os.getcwd() + '/Data/BrainMRI_4.jpg'
    brain1 = plt.imread(image_filename1)
    if len(brain1.shape) > 2:
        brain1 = rgb_to_grayscale(brain1)
    brain2 = plt.imread(image_filename2)
    if len(brain2.shape) > 2:
        brain2 = rgb_to_grayscale(brain2)
    brain3 = plt.imread(image_filename3)
    if len(brain3.shape) > 2:
        brain3 = rgb_to_grayscale(brain3)
    brain4 = plt.imread(image_filename4)
    if len(brain4.shape) > 2:
        brain4 = rgb_to_grayscale(brain4)
    # Recaler les images
    print("Recalage de BrainMRI_2 sur BrainMRI_1")
    descente_gradient(brain1, brain2)
    print("Recalage de BrainMRI_3 sur BrainMRI_1")
    descente_gradient(brain1, brain3)
    print("Recalage de BrainMRI_4 sur BrainMRI_1")
    descente_gradient(brain1, brain4)


def test_recalage_optim():
    image_filename1 = os.getcwd() + '/Data/BrainMRI_1.jpg'
    image_filename2 = os.getcwd() + '/Data/BrainMRI_2.jpg'
    image_filename3 = os.getcwd() + '/Data/BrainMRI_3.jpg'
    image_filename4 = os.getcwd() + '/Data/BrainMRI_4.jpg'
    brain1 = plt.imread(image_filename1)
    if len(brain1.shape) > 2:
        brain1 = rgb_to_grayscale(brain1)
    brain2 = plt.imread(image_filename2)
    if len(brain2.shape) > 2:
        brain2 = rgb_to_grayscale(brain2)
    brain3 = plt.imread(image_filename3)
    if len(brain3.shape) > 2:
        brain3 = rgb_to_grayscale(brain3)
    brain4 = plt.imread(image_filename4)
    if len(brain4.shape) > 2:
        brain4 = rgb_to_grayscale(brain4)
    # Recaler les images
    print("Recalage de BrainMRI_2 sur BrainMRI_1")
    descente_gradient_optim(brain1, brain2, fenetre_ssd=8)
    print("Recalage de BrainMRI_3 sur BrainMRI_1")
    descente_gradient_optim(brain1, brain3, fenetre_ssd=8)
    print("Recalage de BrainMRI_4 sur BrainMRI_1")
    descente_gradient_optim(brain1, brain4, fenetre_ssd=8, eps_rot=0.05)

#test_rotation()
#test_translation()
#test_recalage()
test_recalage_optim()
