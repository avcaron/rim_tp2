15 119 382 - Alex Valcourt Caron
15 101 783 - Marie-Pier Desharnais
15 053 219 - Sami Chibani

NOTA BENE
---------

Version Python : 3.5.4

    Inclure le repertoire TP2 dans le pythonpath pour pouvoir
    executer certaines portions du TP. Quelques interfaces conti-
    ennent des executables en ligne de commande, qui elles utili-
    sent les packages du projet.

    Certaines interfaces utilisent directement les images source
    lors de l'execution. Veiller a les placer dans un repertoire nom-
    me Data, place a la racine du projet.

IMN530 - Reconstruction et analyse dʼimage médicale
===================================================

Disposition des numeros dans le code :

    Les interfaces vers les numeros se trouvent toutes
    dans les fichier .py intitulés Q#.py a la racine du projet.

Numero 1 :
----------
- La fonction calculant l'histogramme conjoint se trouve dans
  **recalage.metrics.histogram.py**.

Numero 2 :
----------

- Les fonctions demandees en a, b et c se trouvent toutes dans
  **recalage.metrics**.

Numero 3 :
----------

- Le code produisant les transformations peut être trouver dans le
  package **recalage.transformation**.

- Les fonctions *trans_rigide* et *similitude* demandees peuvent
  etre trouvees dans le fichier **transformation.py**

Numero 4 :
----------

- Les fonction produisant les images resultantes d'une rotation ou d'une
  translation se trouvent dans le package **recalage.transformation**,
  dans les fichiers *rotation.py* et *translation.py*. Les fonctions se
  nomment **rotation_image** et **translation_image**.

- Les fonctions de recalage se trouvent dans **recalage.recalage.py**.

- Le code de la descente de gradient se trouve dans le package
  **recalage.gradient_descent**.