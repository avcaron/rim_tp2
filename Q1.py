from argparse import ArgumentParser

from recalage.metrics.cr import cr
from recalage.metrics.histogram import jointHist

from matplotlib import pyplot, colors
from mpl_toolkits.axes_grid1 import make_axes_locatable

from scipy.ndimage import imread
import numpy as np

from recalage.metrics.im import im
from recalage.metrics.ssd import ssd
from recalage.utils import rgb_to_grayscale

DESCRIPTION = """
    Fonction de creation de l'histogramme conjoint des deux images donnees en entree.
"""


def create_argument_parser():
    p = ArgumentParser("hist_conjoint", description=DESCRIPTION)
    p.add_argument("image",
                   nargs=2,
                   help="""
                       Images d'entree pour la creation de l'histogramme
                       (doivent etre de meme dimension)
                   """,
                   action="append",
                   type=list
                   )

    p.add_argument("bins", nargs=1, type=int)
    p.add_argument("--metrics", action="store_true", dest="metrics")
    p.add_argument("--as-one", action="store_true", dest="as_one")
    return p


def creer_histogramme_conjoint(image1, image2, bins, as_one=False):

    hist, i1_max, i2_max = jointHist(image1, image2, bins)

    fig, axe = pyplot.subplots()

    divider = make_axes_locatable(axe)
    caxe = divider.append_axes("right", size="5%", pad=0.05)

    axe.set_title("Histogramme conjoint")
    pcm = axe.pcolormesh(
        hist,
        cmap=pyplot.cm.jet,
        norm=colors.SymLogNorm(vmin=0, vmax=hist.max(), linthresh=0.03, linscale=0.03),
    )

    axe.set_xlabel("Image 2")
    axe.set_ylabel("Image 1")

    axe.xaxis.set_major_formatter(pyplot.FuncFormatter(lambda v, t: int(v * i1_max / bins) if t else 0))
    axe.yaxis.set_major_formatter(pyplot.FuncFormatter(lambda v, t: int(v * i2_max / bins) if t else 0))
    axe.set_aspect('equal', 'box')
    axe.set_xbound(0, bins)
    axe.set_ybound(0, bins)

    fig.colorbar(pcm, cax=caxe)

    fig.tight_layout()

    if args.metrics:
        if np.prod(image1.shape) == int(np.sum(hist)):
            print("L'histogramme est conforme")

        print("La somme des differences au carre : {0}".format(ssd(image1, image2)))
        print("Le coefficient de correlation : {0}".format(cr(image1, image2)))
        print("Le coefficient d'information mutuelle : {0}".format(im(hist)))

    if as_one:
        ax2 = divider.new_vertical(size="100%", pad=0.5)
        ax3 = divider.new_vertical(size="100%", pad=0.5)
        fig.add_axes(ax2)
        fig.add_axes(ax3)

        ax2.set_title("Image 2")
        ax3.set_title("Image 1")
        ax2.imshow(image2, cmap="gray", origin="upper")
        ax3.imshow(image1, cmap="gray", origin="upper")
        ax2.set_aspect('equal', 'box')
        ax3.set_aspect('equal', 'box')

    pyplot.axis("equal")

    pyplot.show()

if __name__ == "__main__":
    args = create_argument_parser().parse_args()

    image1 = imread("".join(args.image[0][0]))
    image2 = imread("".join(args.image[0][1]))

    if len(image1.shape) > 2:
        image1 = rgb_to_grayscale(image1)
    if len(image2.shape) > 2:
        image2 = rgb_to_grayscale(image2)

    print("Images shapes : {0} | {1}".format(image1.shape, image2.shape))
    assert image1.shape == image2.shape

    if not args.as_one:
        fig, axes = pyplot.subplots(1, 2)
        axes[0].set_title("Image 1")
        axes[1].set_title("Image 2")
        axes[0].imshow(image1, cmap="gray")
        axes[1].imshow(image2, cmap="gray")

    creer_histogramme_conjoint(image1, image2, args.bins[0], args.as_one)
